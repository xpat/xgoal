<?php


namespace App\Controller;


use App\Entity\Goal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GoalController
 * @package App\Controller
 * @Route("/api/goals", name="api_goal_")
 */
class GoalController extends AbstractController
{

    /**
     * @Route("/create", name="create", methods={"POST"})
     * @param Request $request
     */
    public function createAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        dump($data);
        die;

    }

}