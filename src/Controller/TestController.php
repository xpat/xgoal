<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package App\Controller\Api
 * @Route("/test")
 */
class TestController extends AbstractController
{

    /**
     * @return Response
     * @Route("")
     */
    public function indexAction() {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
}