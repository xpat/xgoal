<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Goal;
use App\Entity\GoalStatus;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class GoalSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;
    private User $user;

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * The code must not depend on runtime state as it will only be called at compile time.
     * All logic depending on runtime state must be put into the individual methods handling the events.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [ 'preWrite', EventPriorities::PRE_WRITE ],
        ];
    }

    public function preWrite(ViewEvent $event) {
        /** @var Goal $goal */
        $goal = $event->getControllerResult();
        $goal->setStatus($this->em->getReference(GoalStatus::class,GoalStatus::STATUS_CREATED));
        $goal->setUser($this->user);
    }

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->user = $security->getUser();
        $this->em = $em;
    }
}