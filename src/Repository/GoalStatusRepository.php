<?php

namespace App\Repository;

use App\Entity\GoalStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GoalStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method GoalStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method GoalStatus[]    findAll()
 * @method GoalStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GoalStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GoalStatus::class);
    }

    // /**
    //  * @return GoalStatus[] Returns an array of GoalStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GoalStatus
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
